package ru.rife.mcarch.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@Entity
@Table(name = "USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "LOGIN", nullable = false)
    private String login;
    @Column(name = "EMAIL", length = 150, nullable = false)
    private String email;
    @Column(name = "GOOGLE_EMAIL", length = 150, nullable = false)
    private String googleEmail;
    @Column(name = "PASSWORD", length = 150)
    private String password;
    @Column(name = "PREV_PASSWORD", length = 150)
    private String prevPassword;
    @Enumerated(EnumType.STRING)
    @Column(name = "CREATED_BY_SYSTEM", length = 50, updatable = false, nullable = false)
    private CreatedBySystem createdBySystem;
    @Column(name = "CREATED_AT", nullable = false)
    private LocalDateTime createAt;
    @Column(name = "FIRST_AUTH", nullable = false)
    private Boolean firstAuth;

    public User(String login,
                String password,
                String email,
                CreatedBySystem createdBySystem) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.createdBySystem = createdBySystem;
        this.firstAuth = true;
        this.createAt = LocalDateTime.now();
    }

    public void changePassword(String newPassword) {
        this.prevPassword = this.password;
        this.password = newPassword;
    }

    public void setGoogleEmail(String googleEmail) {
        this.googleEmail = googleEmail;
    }

    public void changeEmail(String email) {
        this.email = email;
    }
}