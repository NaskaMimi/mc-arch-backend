package ru.rife.mcarch.entity;

public enum CreatedBySystem {
    SELF_REGISTERED,
    GOOGLE_OATH2
}
