package ru.rife.mcarch.dto;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Value
public class CreateUserRequest {
    @NotNull
    @Email
    String email;
    @NotNull
    @Size(min = 5)
    String login;
}
