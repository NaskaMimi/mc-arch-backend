package ru.rife.mcarch.dto.exception;

import lombok.Value;
import java.util.List;

@Value
public class ValidationErrorDto {
    List<ValidationError> errors;
}
