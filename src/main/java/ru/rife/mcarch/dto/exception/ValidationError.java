package ru.rife.mcarch.dto.exception;

import lombok.Value;

@Value
public class ValidationError {
    String source;
    String description;
}
