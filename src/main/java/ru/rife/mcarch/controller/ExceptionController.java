package ru.rife.mcarch.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ru.rife.mcarch.dto.exception.ValidationError;
import ru.rife.mcarch.dto.exception.ValidationErrorDto;
import ru.rife.mcarch.exception.RedirectException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(RedirectException.class)
    public ResponseEntity<Void> handleRedirectException(RedirectException redirectException) {
        UriComponents uriComponents = UriComponentsBuilder.fromUri(redirectException.getRedirectUrl())
                .queryParams(redirectException.getParams())
                .build();
        return ResponseEntity.status(HttpStatus.SEE_OTHER)
                .location(uriComponents.toUri())
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        List<ValidationError> errors = exception.getBindingResult().getAllErrors().stream()
                .map(this::objectErrorToMessage)
                .toList();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ValidationErrorDto(errors));
    }

    private ValidationError objectErrorToMessage(ObjectError error) {
        if (error instanceof FieldError fError) {
            return new ValidationError(fError.getField(), fError.getDefaultMessage());
        }
        return new ValidationError(null, error.getDefaultMessage());
    }
}