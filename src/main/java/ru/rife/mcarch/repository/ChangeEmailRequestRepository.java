package ru.rife.mcarch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rife.mcarch.entity.ChangeEmailRequestEntity;

import java.util.UUID;

public interface ChangeEmailRequestRepository extends JpaRepository<ChangeEmailRequestEntity, UUID> {}