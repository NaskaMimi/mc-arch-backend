package ru.rife.mcarch.security.oath2;

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import ru.rife.mcarch.security.JwtUtil;

public interface JwtTokenProvider {
    JwtUtil.JwtRich jwtTokenGenerate(OAuth2AuthenticationToken oAuth2AuthenticationToken);

    boolean equalsSystemId(OAuth2AuthenticationToken oAuth2AuthenticationToken);
}
