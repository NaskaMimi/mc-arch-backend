package ru.rife.mcarch.security.oath2;

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Component;
import ru.rife.mcarch.security.JwtUtil;

import java.util.Map;

@Component
public class GoogleJwtTokenProvider implements JwtTokenProvider {
    private final JwtUtil jwtUtil;

    public GoogleJwtTokenProvider(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Override
    public JwtUtil.JwtRich jwtTokenGenerate(OAuth2AuthenticationToken oAuth2AuthenticationToken) {
        Map<String, Object> attributes = oAuth2AuthenticationToken.getPrincipal().getAttributes();
        var email = (String) attributes.get("email");
        return jwtUtil.generateToken(email);
    }

    @Override
    public boolean equalsSystemId(OAuth2AuthenticationToken oAuth2AuthenticationToken) {
        return "google".equals(oAuth2AuthenticationToken.getAuthorizedClientRegistrationId());
    }
}
