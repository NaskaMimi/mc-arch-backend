package ru.rife.mcarch.security;

public final class RoleConstants {
    public static final String GUEST = "GUEST";
    public static final String USER = "USER";
    private RoleConstants() {}
}