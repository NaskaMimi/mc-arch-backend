package ru.rife.mcarch.exception;

import lombok.Getter;

@Getter
public class ControlFailedException extends RuntimeException {
    private final ErrorInfo errorInfo;

    public ControlFailedException(String message, ErrorInfo errorInfo) {
        super(message);
        this.errorInfo = errorInfo;
    }

    public ControlFailedException(String message, Throwable cause, ErrorInfo errorInfo) {
        super(message, cause);
        this.errorInfo = errorInfo;
    }
}
