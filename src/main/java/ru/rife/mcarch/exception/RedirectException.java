package ru.rife.mcarch.exception;

import lombok.Getter;
import org.springframework.util.MultiValueMap;

import java.net.URI;

@Getter
public class RedirectException extends RuntimeException {
    private final URI redirectUrl;
    private final MultiValueMap<String, String> params;

    public RedirectException(String message,
                             URI redirectUrl,
                             MultiValueMap<String, String> params) {
        super(message);
        this.redirectUrl = redirectUrl;
        this.params = params;
    }

    public RedirectException(String message, Throwable cause,
                             URI redirectUrl, MultiValueMap<String, String> params) {
        super(message, cause);
        this.redirectUrl = redirectUrl;
        this.params = params;
    }
}
